define([
    'underscore',
    'marionette',
    
    'views/target_telemetry_pane',

    'pallete'

], function(_,
    Marionette,
    TargetTelemetryPane,
    pallete) {

    // Extending Group layer
    // http://leafletjs.com/examples/extending/extending-2-layers.html
    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: function(map) {
            var self = this;

            L.LayerGroup.prototype.onAdd.call(this, map);

            if (!self.initialized) {
                self._map.on('moveend', self.updateBounds.bind(self));
                self.options.collection.on('add', self.entityAdded.bind(self));
                self.options.collection.on('remove', self.entityRemoved.bind(self));
                self.options.collection.on('change', self.entityChanged.bind(self));
                self.options.collection.on('selected', self.entitySelected.bind(self));

                self.updateBounds();

                self.initialized = true;
            }

            self.options.collection.startMonitoring();
        },

        onRemove: function(map) {
            var self = this;

            self.options.collection.stopMonitoring();

            var model;

            while (model = self.options.collection.first()) {
                self.options.collection.remove(model);
            }

            L.LayerGroup.prototype.onRemove.call(self, map);
        },

        updateBounds: function() {
            var self = this;

            if (!self._map) {
                return;
            }

            var bounds = self._map.getBounds();
            self.options.collection.bounds = {
                south: bounds.getSouthWest().lat,
                west: bounds.getSouthWest().lng,
                north: bounds.getNorthEast().lat,
                east: bounds.getNorthEast().lng
            };
        },

        entityAdded: function(model, collection) {
            var self = this;
            var pos = L.latLng(model.get('position').lat, model.get('position').lon);

            model.marker = L.marker(pos, { icon: self.helperIcon(model) })
                .bindPopup()
                .bindTooltip('<b>' + model.get('name') + '</b><br>' + model.get('model'), {
                    permanent: false,
                    offset: [0, 0],
                    tooltipAnchor: [0, 0]
                })
                .addTo(self);

            if (model.get('type') === 'uav') {
                model.marker.setRotationOrigin('center center');
                model.marker.setRotationAngle(model.get('position').hdg - 90.0);
            }

            model.marker.on('popupopen', function(e) {

                var TargetPaneLayout = Marionette.LayoutView.extend({
                    template: _.template('\
                        <b><%= name %></b><br>\
                        <%= model %>\
                        <div id="telemetry"></div>\
                    '),
                    regions: {
                        telemetry: "#telemetry"
                    },

                    events: {
                    }
                });

                model.marker.layout = new TargetPaneLayout({
                    model: model // target model
                });
                model.marker.layout.render();

                e.popup.setContent(model.marker.layout.el);

                model.trigger('popup', model);
            })

            model.marker.on('popupclose', function(e) {
                model.marker.layout.destroy();
            })

            model.polyline = new L.Polyline([], {
                color: pallete.getColor(model.get('id')),
                weight: 3,
                opacity: 0.9
            })

            model.polyline.addTo(self);

            self.maybeHighlightTarget(model);
        },

        maybeHighlightTarget: function(model) {
            var targetId = getQueryVariable('target');
            if (targetId && targetId === model.get('id')) {
                var desirableSource = getQueryVariable('channel');
                if (desirableSource) {
                    model.set('source', new Backbone.Model({
                        name: desirableSource
                    }));
                }

                model.marker.openPopup();
            }

            function getQueryVariable(variable) {
               var query = window.location.search.substring(1);
               var vars = query.split("&");
               for (var i=0;i<vars.length;i++) {
                       var pair = vars[i].split("=");
                       if(pair[0] == variable){return pair[1];}
               }
               return;
            }
        },

        entityRemoved: function(model, collection) {
            var self = this;

            if (model.marker) {
                self.removeLayer(model.marker);
            }

            if (model.polyline) {
                self.removeLayer(model.polyline);
            }
        },

        entityChanged: function(model) {
            var pos = L.latLng(model.get('position').lat, model.get('position').lon);
            model.marker.setLatLng(pos);

            model.polyline.addLatLng(pos);

            if (model.polyline.getLatLngs().length > 500) {
                model.polyline.spliceLatLngs(0, 1);
            }

            // set green icon to indicate that the target is active
            model.marker.setIcon(this.helperIcon(model));
            
            if (model.get('type') === 'uav') {
                model.marker.setRotationAngle(model.get('position').hdg - 90.0);
            }
        },

        entitySelected: function(model) {
            var self = this;

            if (!self._map) {
                return;
            }

            if (self.selectedTarget && self.selectedTarget.marker) {
                self.selectedTarget.marker.closePopup();
            }

            this.selectedTarget = model;
            this.selectedTarget.marker.openPopup();

            var pos = L.latLng(
                this.selectedTarget.get('position').lat, 
                this.selectedTarget.get('position').lon);

            self._map._zoom = 20;
            self._map.panTo(pos);
        },

        helperIcon: function(model) {
            var ts = new Date().getTime();
            var updated = ts - model.get('updated') < 10 * 60 * 1000; // updated within last 10 minutes
            var url = updated 
                ? 'img/targets/' + model.get('type') + '-green.svg'
                : 'img/targets/' + model.get('type') + '.svg';

            return new L.icon({
                iconUrl: url,
                iconSize: [36, 36],
                iconAnchor: [18, 18],
                popupAnchor: [0, -18],
                shadowUrl: null
            })
        },
    });
});
