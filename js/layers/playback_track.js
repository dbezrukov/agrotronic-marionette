define([
    'underscore',
    'Leaflet.MultiOptionsPolyline'

], function(_) {

    L.Control.ModeLegend = L.Control.extend({
        onAdd: function(map) {
            var div = L.DomUtil.create('div', 'legend');

            div.innerHTML = '\
            <div style="background-color: #f6f6f6; padding: 10px; display: flex; align-items: center; justify-content: flex-start; font-size: 14px;">\
                Режимы:\
                <span><i style="color: #06a9f5;" class="glyphicon glyphicon-stop"></i>Движение</span>\
                <span><i style="color: #78c800;" class="glyphicon glyphicon-stop"></i>Комбайнирование</span>\
            </div>';

            return div;        
        },
        maybeShow: function(map) {
            L.Control.ModeLegend.showCounter++;
            if (L.Control.ModeLegend.showCounter === 1) {
                this.addTo(map);
            }
        },
        maybeHide: function(map) {
            L.Control.ModeLegend.showCounter--;
            if (L.Control.ModeLegend.showCounter === 0) {
                this.remove();
            }
        }
    });

    L.Control.SpeedLegend = L.Control.extend({
        onAdd: function(map) {
            var div = L.DomUtil.create('div', 'legend');

            div.innerHTML = '\
            <div style="background-color: #f6f6f6; padding: 10px; display: flex; align-items: center; justify-content: flex-start; font-size: 14px;">\
                Скорость, км/час:\
                <span><i style="color: #202020;" class="glyphicon glyphicon-stop"></i>0</span>\
                <span><i style="color: #06a9f5;" class="glyphicon glyphicon-stop"></i>3</span>\
                <span><i style="color: #78c800;" class="glyphicon glyphicon-stop"></i>6</span>\
                <span><i style="color: #fb8b14;" class="glyphicon glyphicon-stop"></i>9</span>\
                <span><i style="color: #d10a40;" class="glyphicon glyphicon-stop"></i>12</span>\
            </div>';

            return div;        
        },
        maybeShow: function(map) {
            L.Control.SpeedLegend.showCounter++;
            if (L.Control.SpeedLegend.showCounter === 1) {
                this.addTo(map);
            }
        },
        maybeHide: function(map) {
            L.Control.SpeedLegend.showCounter--;
            if (L.Control.SpeedLegend.showCounter === 0) {
                this.remove();
            }
        }
    });

    L.Control.LoadLegend = L.Control.extend({
        onAdd: function(map) {
            var div = L.DomUtil.create('div', 'legend');

            div.innerHTML = '\
            <div style="background-color: #f6f6f6; padding: 10px; display: flex; align-items: center; justify-content: flex-start; font-size: 14px;">\
                Нагрузка двигателя:\
                <span><i style="color: #06a9f5;" class="glyphicon glyphicon-stop"></i>25</span>\
                <span><i style="color: #78c800;" class="glyphicon glyphicon-stop"></i>50</span>\
                <span><i style="color: #fb8b14;" class="glyphicon glyphicon-stop"></i>75</span>\
                <span><i style="color: #d10a40;" class="glyphicon glyphicon-stop"></i>100</span>\
            </div>';

            return div;        
        },
        maybeShow: function(map) {
            L.Control.LoadLegend.showCounter++;
            if (L.Control.LoadLegend.showCounter === 1) {
                this.addTo(map);
            }
        },
        maybeHide: function(map) {
            L.Control.LoadLegend.showCounter--;
            if (L.Control.LoadLegend.showCounter === 0) {
                this.remove();
            }
        }
    });

    L.Control.ModeLegend.showCounter = 0;
    L.Control.SpeedLegend.showCounter = 0;
    L.Control.LoadLegend.showCounter = 0;

    var viewOptions = {
        mode: {
            legend: function(opts) {
                return new L.Control.ModeLegend(opts);
            },
            multiOptions: {
                optionIdxFn: function (latLng) {
                    const mode = latLng.meta.mode ? latLng.meta.mode.toLowerCase() : '';
                    if (mode === 'm1') {
                        return 0;
                    } else if (mode === 'm4' || mode === 'm5') {
                        return 1;
                    } else {
                        return 2;
                    }
                },
                options: [
                    { color: '#06a9f5' },  // drive
                    { color: '#78c800' },  // harvesting
                    { color: '#202020' },  // na
                ]
            }
        },
        speed: {
            legend: function(opts) {
                return new L.Control.SpeedLegend(opts);
            },
            multiOptions: {
                optionIdxFn: function (latLng) {
                    const speed = latLng.meta.speed;
                    const zones = [0, 3, 6, 9, 12];

                    for (var i = zones.length - 1; i >=0;  --i) {
                        if (speed >= zones[i]) {
                            return i;
                        }
                    }
                    return 0;
                },
                options: [
                    { color: '#202020' },
                    { color: '#06a9f5' },
                    { color: '#78c800' },
                    { color: '#fb8b14' },
                    { color: '#d10a40' }
                ]
            }
        },
        load: {
            legend: function(opts) {
                return new L.Control.LoadLegend(opts);
            },
            multiOptions: {
                optionIdxFn: function (latLng) {
                    const load = latLng.meta.load || 0;

                    const zones = [25, 50, 75, 200];

                    for (var i = 0; i < zones.length;  ++i) {
                        if (load < zones[i]) {
                            return i;
                        }
                    }
                    return 0;
                },
                options: [
                    { color: '#06a9f5' },
                    { color: '#78c800' },
                    { color: '#fb8b14' },
                    { color: '#d10a40' }
                ]
            }
        }
    }

    // Extending Group layer
    // http://leafletjs.com/examples/extending/extending-2-layers.html
    return L.LayerGroup.extend({

        initialize: function(options) {
            this.options = options;
            this.view = options.view || 'speed';

            L.LayerGroup.prototype.initialize.call(this, options);
        },

        onAdd: function(map) {
            var self = this;

            //console.log('TrackLayer: onAdd');

            L.LayerGroup.prototype.onAdd.call(self, map);

            function getRandomArbitrary(min, max) {
                return Math.random() * (max - min) + min;
            }

            var positions = _.map(self.options.collection.models, function(value) {
                var pos = L.latLng(value.get('lat'), value.get('lon'));
                pos.meta = {
                    mode: value.get('data').mode,
                    speed: value.get('total').speed,
                    load: value.get('data').load
                }
                return pos;
            });

            // show track markers
            self.markers = [];
            var text = ''; 

            text += '<b>' + self.trackViewName(self.view) + '</b><br>';
            text += 'Машина: <b>' + self.options.model.get('name') + '</b><br>';
            text += 'Старт: ' + stringFromUtc(self.options.collection.first().get('utc'));

            addTrackMarker(positions[0], 'img/start.svg', text);

            text = '';
            text += '<b>' + self.trackViewName(self.view) + '</b><br>';
            text += 'Машина: <b>' + self.options.model.get('name') + '</b><br>';
            text += 'Финиш: ' + stringFromUtc(self.options.collection.last().get('utc'));

            addTrackMarker(positions.slice(-1)[0], 'img/finish.svg', text);

            // new polyline
            this.polyline = L.multiOptionsPolyline(positions, {
                multiOptions: viewOptions[this.view].multiOptions,
                weight: 3,
                lineCap: 'round',
                opacity: 0.75,
                smoothFactor: 1
            });

            this.polyline.addTo(map);
            map.fitBounds(this.polyline.getBounds());

            // show legend
            this.legend = viewOptions[this.view].legend({
                position: 'bottomright'
            })
            this.legend.maybeShow(map);
            
            function addTrackMarker(pos, iconUrl, text) {
                if (!pos) {
                    return;
                }

                self.markers.push(L.marker(pos, {
                    icon: new L.icon({
                        iconUrl: iconUrl,
                        iconSize: [40, 40],
                        iconAnchor: [10, 34],
                        shadowUrl: null
                    })
                })
                .bindTooltip(text, {
                    permanent: false,
                    offset: [0, -25]
                })
                .addTo(map));
            }

            function stringFromUtc(utc) {
                var date = new Date(utc);
                var updatedLocal = date.toLocaleString();
                return updatedLocal.substr(0, updatedLocal.lastIndexOf(':'));
            }
        },

        onRemove: function(map) {

            //console.log('TrackLayer: onRemove');
            
            this.markers.forEach(function(marker) {
                map.removeLayer(marker);
            })

            if (this.polyline) {
                map.removeLayer(this.polyline);
            }

            if (this.legend) {
                this.legend.maybeHide();
            }

            L.LayerGroup.prototype.onRemove.call(this, map);
        }, 

        trackViewName: function(view) {
            if (view === 'mode') {
                return 'Трек по режимам';
            } else if (view === 'speed') {
                return 'Трек по cкорости';
            } if (view === 'load') {
                return 'Трек по нагрузке';
            } else {
                return 'Неизвестный тип трека';
            }
        }
    });
});