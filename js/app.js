define([

    'jquery',
    'underscore',
    'marionette',

    'application/application',
    'views/navbar_widget'

], function($, _, Marionette, 

	Application,
    NavbarWidget) {

	var app = new Application();
    window.app = app;

    app.on("start", function() {

	  	console.log('Initilizing WebGIS app');

        $('body').append(app.layout.render().el);
	  	
	  	var modes = [/*
            '<li>\
                <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" target="_blank" href="https://droneradar.org/api">Документация по API</a>\
                <a class="hidden-xs" target="_blank" href="https://droneradar.org/api">Документация по API</a>\
            </li>'
        */]

		var accountWidget = '';

        if (app.account.get('ticket')) {
            accountWidget = '<li><a onclick="app.account.logout()" style="cursor: pointer">Выйти</a></li>';

        } else {
            accountWidget = '<li><a href="https://harvester.rostselmash.com:8443/cas/login?service=' + location.origin + location.pathname + '&style=rsm">Войти</a></li>';
        }

        var widgets = [
            _.template(accountWidget)(app.account.toJSON())
        ]

	  	app.layout.navbar.show(new NavbarWidget({
	  		model: app.account,
	  		modes: modes,
	  		widgets: widgets
	  	}));

	  	app.route = new Route();

        Backbone.history.start({
            pushState: true,
            root: '/radar'
        });
    });

    var Route = Backbone.Marionette.AppRouter.extend({
        routes : {
            '': 'goMain'
        },
        goMain: function() {
            require(['views/main_pane'], (MainPane) => {
                app.layout.content.show(new MainPane({
                    model: app.account
                }));
            })
        }
    });

    return app;
});
