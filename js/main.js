require.config({
    waitSeconds: 30,
    baseUrl: 'js/',
    urlArgs: 'ver=1.1',

    paths: {
        'jquery': 'bower_components/jquery/jquery.min',
        'jquery.serializeobject': 'vendors/jquery.serializeobject',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
        'bootstrap-datetimepicker': 'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min',
        'underscore': 'bower_components/underscore/underscore-min',
        'sprintfjs': 'bower_components/sprintf/dist/sprintf.min',
        'backbone': 'bower_components/backbone/backbone-min',
        'marionette': 'bower_components/marionette/lib/backbone.marionette.min',
        'tpl': 'bower_components/requirejs-tpl/tpl',
        'pallete': 'vendors/pallete',
        'moment': 'bower_components/moment/min/moment-with-locales.min',
        'js-cookie': 'bower_components/js-cookie/src/js.cookie',
        'leaflet': 'bower_components/leaflet/dist/leaflet',
        'Leaflet.MultiOptionsPolyline': 'bower_components/Leaflet.MultiOptionsPolyline/Leaflet.MultiOptionsPolyline.min',
        'leaflet-groupedlayercontrol': 'bower_components/leaflet-groupedlayercontrol/dist/leaflet.groupedlayercontrol.min'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'leaflet': {
            exports: 'L'
        },
        'Leaflet.MultiOptionsPolyline': {
            deps: [ 'leaflet' ],
        },
        'leaflet-groupedlayercontrol': {
            deps: [ 'leaflet' ],
        },
        'moment': {
            deps: [],
        },
        'jquery.serializeobject': {
	    	deps: [ 'jquery' ],
	    	exports: '$.fn.serializeObject'
		},
        'highcharts': {
            deps: ['jquery'],
        },
        'bootstrap': {
            deps: ['jquery'],
        },
        'bootstrap-datetimepicker': {
            deps: ['jquery', 'bootstrap', 'moment'],
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: "Marionette"
        },
        'underscore': {
            exports: '_'
        }
    },
    map: {
        '*': {
            'css': 'bower_components/require-css/css.min'
        }
    }
});

require([

    'jquery',
    'app',

    'models/account',

    'bootstrap',
    'leaflet',

    'css!bower_components/bootstrap/dist/css/bootstrap.min.css',
    'css!bower_components/leaflet/dist/leaflet.css',
    'css!stylesheets/common_style'

 ], function($, app, Account) {

    $(function() {

        var urlParams = new URLSearchParams(window.location.search);
        if (urlParams.has('ticket')) {
            window.localStorage.setItem('rsm-ticket', urlParams.get('ticket'));

            // remove the ticket parameter
            window.history.replaceState(null, null, window.location.pathname);
        }

        app.account = new Account();

        if (!app.account.get('ticket')) {
            console.log('No rsm service ticket found, redirecting to CAS');
            window.location.href = 'https://harvester.rostselmash.com:8443/cas/login?service=' + location.origin + location.pathname + '&style=rsm';
            return;
        }

        console.log('service token is: ' + app.account.get('ticket'));

        app.api = 'https://harvester.rostselmash.com:444/api/droneradar';

        window.app = app;
		
        // profile API is not yet implemented
        //app.account.fetch({
		//	success: function() {
				app.start({});
		//	}
		//});
    });
});
