define([], function() {

    var pallete = {
        
        colors: [
            'lightblue',
            'lightgreen',
            'lightcoral',
            'navajowhite',
            'burlywood'
        ],

        getColor: function(id) {
            var self = this;

            if (!self.registeredColors) {
                self.registeredColors = {};
            }

            if (!self.lastColor) {
                self.lastColor = 0;
            }

            var color;

            if (id in self.registeredColors === true) {
                color = self.registeredColors[id];
            } else {
                if (self.lastColor === self.colors.length) {
                    self.lastColor = 0;
                }

                color = self.colors[self.lastColor];
                self.registeredColors[id] = color;

                self.lastColor++;
            }
            return color;
        }
    }

    return pallete;
});
