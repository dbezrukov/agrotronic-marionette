define([
    'jquery',
    'marionette'
    
], function($, Marionette) {

	var ModalRegion = Marionette.Region.extend({
        el: '.modal',

        constructor: function() {
            Marionette.Region.prototype.constructor.apply(this, arguments);
     
            //this.ensureEl();
            this.$el.on('hidden', {region:this}, function(event) {
                event.data.region.close();
            });
        },
     
        onShow: function(view) {
            view.on('close', this.onClose, this);
            this.$el.modal('show');
        },
     
        onClose: function() {
            this.$el.modal('hide');
        }
    });

	return Marionette.LayoutView.extend({
  		template: '#layout-template',
  		regions: {
  			top: '#top-region',
    		navbar: '#navbar-region',
    		content: '#content-region',
    		footer: '#footer-region',
    		modal: ModalRegion
  		},
  		id: 'region-wrapper'
	});
});
