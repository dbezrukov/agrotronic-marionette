define([
    'jquery',
    'marionette',
    './layout',
    
], function($, Marionette, Layout) {

    return Marionette.Application.extend({
	  	
	  	onBeforeStart: function() {
	  		var self = this;

	  		app.layout = new Layout();

	    	self.navigate = function(route, options) {
		  		options || (options = {});
		  		Backbone.history.navigate(route, options);
			};

			self.trackPage = function() {
				var page = Backbone.history.root + Backbone.history.fragment;
				
				self.vent.trigger("page:change", page);

			};

			Backbone.history.on('route', function(router, route, params) {
		  		return self.trackPage();
			});

	    	$(document).on('click', 'a', function (e) {

			    var href = $(this).attr('href');

                // skip empty link
			    if (!href) {
			    	e.preventDefault();
			    	return;
			    }

                // allow all extern links
                if (href.indexOf('//') >= 0 || href.indexOf('tel:') >= 0) {
                    return;   
                }

                // allow root from anywhere
                if (href === '/') {
                    return;   
                }

                // allow logout from anywhere
                if (href.indexOf('/logout') >= 0) {
                    return;   
                }

                // allow redirects to auth page
                if (window.location.href.indexOf('/auth') < 0 && href.indexOf('/auth') >= 0) {
                    return;
                }

                // allow redirects to admin page
                if (window.location.href.indexOf('/admin') < 0 && href.indexOf('/admin') >= 0) {
                    return;
                }

			    // follow internal link
			    e.preventDefault();
			    self.navigate(href, { trigger: true });
			});
  		}
	});
});
