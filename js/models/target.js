define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: 'id',
        urlRoot: app.api + '/targets',
        
        defaults: function() {
            return {
                valid: false,
                updated: 0,
                type: 'uav',
                
                position: {
                    lat: 0,
                    lon: 0,
                    utc: 0,
                    hgt: 0,
                    hdg: 0,
                    speed: 0,
                    data: {
                    },
                    total: {
                    }
                },

                showTelemetry: false,
                showTrack: false,
                track: null
            }
        },

        parse: function (response) {

            // Не сбрасывать data.speed в течении пол минуты
            // data.speed приоритетней чем speed
            // console.log(response);

            response.position.total = {
                speed: response.position.data.speed === undefined ? response.position.speed : response.position.data.speed,
                hdg: response.position.data.hdg === undefined ? response.position.hdg : response.position.data.hdg
            }

            return response;
        }
    });
});
