define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({

        defaults: function() {
            return {
                lat: 0,
                lon: 0,
                utc: 0,
                hgt: 0,
                hdg: 0,
                speed: 0,
                data: {
                },
                total: {
                }
            }
        }, 

        parse: function (response) {

            // Не сбрасывать data.speed в течении пол-минуты
            // data.speed приоритетней чем speed
            
            response.total = {
                speed: response.data.speed === undefined ? response.speed : response.data.speed,
                hdg: response.data.hdg === undefined ? response.hdg : response.data.hdg
            }

            return response;
        }

    });
});
