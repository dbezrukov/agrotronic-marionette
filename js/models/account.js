define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        
        //idAttribute: 'id',
        //url: app.api + '/account/profile',

        defaults: function() {
            return {
                "app": "Agrotronic",
                "name": "tc_rsm8",
                "displayName": "RSM Guest",
                "photostorage": "/",
                "photo": "img/user64.png",
                "ticket": window.localStorage.getItem('rsm-ticket')
            }
        },

        logout: function() {
            window.localStorage.removeItem('rsm-ticket');
            window.location.href = 'https://harvester.rostselmash.com:8443/cas/logout?service=' + location.origin + location.pathname + '&style=rsm';
        }
    });
});
