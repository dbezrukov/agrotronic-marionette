define([
    'underscore',
    'sprintfjs',
    'backbone',
    'models/position'

], function(_, sprintf, Backbone, Position) {

    return Backbone.Collection.extend({
        model: Position,

        initialize: function(options) {
            this.options = options;
        },

        fetch: function(options) {
            alert('Track: fetch should not be called');
        },

        startFetching: function(progressCallback) {
            var self = this;

            self.fetchStarted = new Date().getTime();
            self.error = '';

            var urlTemplate = app.api + '/targets/%s/track?from=%i&to=%i';
            var url = sprintf.sprintf(urlTemplate, this.options.targetId, this.options.from, this.options.to);

            var ticket = app.account.get('ticket');
            if (ticket) {
                url += '&ticket=' + ticket;
            }

            fetch(url, {
                mode: 'cors'
            })
            .then(response => consume(response, progressCallback))
            .then(function(response) {
                return response.json();
            })
            .then(function(json) {
                if (json.length === 0) {
                    self.error = '<b>Ошибка загрузки</b><br>Нет данных за указанное время';
                }
                
                //self.reset(json);
                self.reset(_.map(json, (new Position).parse));
            })
            .catch(function(alert) {
                self.error = '<b>Ошибка загрузки</b><br>Нет ответа от сервера';
                self.reset([]);
            })

            function consume(response, progressCallback) {

                self.bytesFetched = 0;
                var clonedResponse = response.clone();
                var reader = clonedResponse.body.getReader();
                
                return pump();
                
                function pump() {
                    return reader.read().then(({done, value}) => {
                        if (done) {
                            return response;
                        }
                        self.bytesFetched += value.byteLength;
                        self.fetchTime = new Date().getTime();
                        
                        progressCallback();
                        return pump();
                    })
                }
            }
        }
    });
});