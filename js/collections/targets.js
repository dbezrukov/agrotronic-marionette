define([
    'sprintfjs',    
    'backbone',
    'models/target'

], function(sprintf, Backbone, Target) {

    var status = function(response) {
        if (response.status >= 200 && response.status < 300) {
            return Promise.resolve(response)
        } else {
            console.log('catching CAS auth error, relogin needed');
            if (response.status === 401) {
                app.account.logout();
            }
            return Promise.reject(new Error(response.statusText))
        }
    }

    var json = function(response) {
        return response.json()
    }

    //https://gist.github.com/akre54/9891fc85ff46afd85814
    //https://developers.google.com/web/updates/2015/03/introduction-to-fetch
    Backbone.ajax = function(options) {

        fetch(options.url, {
            mode: 'cors',
        })
        .then(status)
        .then(json)
        .then(options.success)
        .catch(options.error);
    };

    return Backbone.Collection.extend({
        model: Target,

        //sort_key: 'updated',

        initialize: function() {
            this.since = 0;
        },

        /*
        comparator: function(item) {
            return -item.get(this.sort_key);
        },
        */

        url: function() {
            var url = app.api + '/targets?since=%i';

            var ticket = app.account.get('ticket');
            if (ticket) {
                url += '&ticket=' + ticket;
            }
            
            return sprintf.sprintf(url, this.since);
        },

        parse: function(response) {
            var self = this;

            if (response.targets.length > 0) {
                self.since = response.stm;
            }

            return response.targets;
        },

        startMonitoring: function() {
            var self = this;

            console.log('TARGETS: startMonitoring');
            self.since = 0;

            self.timer = setInterval(function() {

                self.fetch({
                    error: function() {
                        self.forEach(function(entity) {
                            self.remove(entity);
                        })
                    },
                    success: function() {
                    },
                    remove: false
                });
            }, 10000);

            self.fetch();
        },

        stopMonitoring: function() {
            var self = this;

            console.log('TARGETS: stopMonitoring');

            if (self.timer) {
                clearInterval(self.timer);
            }
        },

        bounds: {
            south: 0,
            west: 0,
            north: 0,
            east: 0
        }
    });
});