define([
    'marionette',
    'views/target_item',
    'css!stylesheets/targets_widget'

], function(Marionette, ItemView) {

    return Marionette.CollectionView.extend({
        tagName: 'ul',
        className: 'targets-widget',
        childView: ItemView,
        sort: true,
        
        collectionEvents: {
        },

        initialize: function(options) {
            this.collection.on('popup', this.onTargetPopup.bind(this));
        },

        filter: function (child, index, collection) {
            if (!this.filterValue) {
                return true;
            }

            return child.get('model').toLowerCase().indexOf(this.filterValue.toLowerCase()) >=0
                   || child.get('name').toLowerCase().indexOf(this.filterValue.toLowerCase()) >=0
        },

        filterBy: function(value) {
            this.filterValue = value;
            this.render();
        },

        onTargetPopup: function(model) {
            var first = $(this.el).find('li:first');
            var li = $(this.el).find('li#' + model.get('name'));

            if (li && li.position()) {
                $(this.el).scrollTop(li.position().top - first.position().top);
            }
        }
    });
});