define([
    'marionette',
    'views/target_telemetry_pane',
    'views/target_track_pane',
    'tpl!templates/target_item.tmpl',

], function(
    Marionette, 
    TargetTelemetryPane,
    TargetTrackPane,
    template) {

    var TargetPaneLayout = Marionette.LayoutView.extend({
        template: _.template('\
            <div id="track"></div>\
        <div id="telemetry"></div>'),

        regions: {
            track: "#track",
            telemetry: "#telemetry"
        },

        events: {
        }
    });

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'li',
        
        id: function() {
            return this.model.get('name');
        },
        
        ui: {
            'targetIcon': 'img',
            'layoutWrapper': '.layout-wrapper',
            'buttonTrack': '.trigger-track',
            'buttonTelemetry': '.trigger-telemetry'
        },

        events: {
            'click .select-target': 'selectTarget',
            'click .trigger-telemetry': 'triggerTelemetry',
            'click .trigger-track': 'triggerTrack'
        },

        modelEvents: {
            'change:updated': 'updateIcon'
        },

        templateHelpers: {
            helperIcon: function() {
                var ts = new Date().getTime();
                var updated = ts - this.updated < 10 * 60 * 1000; // updated within last 10 minutes
                return updated 
                    ? 'img/targets/' + this.type + '-green.svg'
                    : 'img/targets/' + this.type + '.svg';
            },
            helperUpdated: function() {
                var date = new Date(this.updated);
                var updatedLocal = date.toLocaleString();
                return updatedLocal.substr(0, updatedLocal.lastIndexOf(':'));
            }
        },

        onRender: function() {
            this.updateIcon();

            this.maybeShowTelemetryPane();
            this.maybeShowTrackPane();
        },
		
        updateIcon: function() {
            var iconUrl = this.templateHelpers.helperIcon.call(this.model.toJSON());
            $(this.ui.targetIcon).attr('src', iconUrl);
        },

        selectTarget: function() {
            this.model.trigger('selected', this.model);
        },

        triggerTelemetry: function() {
            this.model.set('showTelemetry', !this.model.get('showTelemetry'));
            this.maybeShowTelemetryPane();
        },

        triggerTrack: function() {
            this.model.set('showTrack', !this.model.get('showTrack'));
            this.maybeShowTrackPane();
        }, 

        maybeShowTelemetryPane: function() {
            this.ensureLayoutCreated();

            if (this.model.get('showTelemetry')) {
                this.ui.buttonTelemetry.removeClass('btn-default').addClass('btn-info');
                this.layout.telemetry.show(new TargetTelemetryPane({
                    model: this.model // target model
                }));
            } else if (this.layout.telemetry.currentView) {
                this.ui.buttonTelemetry.removeClass('btn-info').addClass('btn-default');
                this.layout.telemetry.reset();
            }
        },

        maybeShowTrackPane: function() {
            this.ensureLayoutCreated();

            if (this.model.get('showTrack')) {
                this.ui.buttonTrack.removeClass('btn-default').addClass('btn-info');
                this.layout.track.show(new TargetTrackPane({
                    model: this.model // target model
                }));
            } else if (this.layout.track.currentView) {
                this.ui.buttonTrack.removeClass('btn-info').addClass('btn-default');
                this.layout.track.reset();
            }
        },

        ensureLayoutCreated: function() {
            
            if (!this.layout) {
                this.layout = new TargetPaneLayout();
                this.ui.layoutWrapper.html(this.layout.render().el);
            }
        }
    });
});