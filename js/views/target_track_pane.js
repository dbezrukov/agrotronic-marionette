define([
    'marionette',
    'collections/track',
    'layers/playback_track',
    'tpl!templates/target_track_pane.tmpl',
    'bootstrap-datetimepicker',
    'css!bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
    'css!stylesheets/sidebar_playback_pane',
    'css!bower_components/font-awesome/css/font-awesome.min.css'

], function(
    Marionette, 
    Track,
    TrackLayer,
    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'target-track-pane no-select',

        ui: {
            'timeFrom': 'input[name="time-from"]',
            'timeTo': 'input[name="time-to"]',
            'status': '.status',
            'load': '.load',
            'unload': '.unload',
            'view': '.view'
        },
        
        events: {
            'click .input-group-addon': 'onClickAddonButton',
            'click .load': 'onClickedLoadTrack',
            'click .unload': 'onClickedUnloadTrack',
            'click .view': 'onClickedTrackView'
        },
        
        modelEvents: {
        },

        templateHelpers: {
        },

        onRender: function() {
            
            // todo: support track loading state
            if (this.model.track) {
                // show previously selected dates and show track
                this.initPlaybackDatePicker(
                    this.model.track.options.from, 
                    this.model.track.options.to);
                this.onTrackReset();
            } else {
                // show default dates and enable track loading
                this.initPlaybackDatePicker();
                this.onStateLoadTrack();
            }
        },

        initPlaybackDatePicker: function(from, to) {
            var today = new Date();

            var weekAgo = new Date();
            weekAgo.setDate(weekAgo.getDate() - 7);
            weekAgo.setHours(0,0,0,0);

            this.ui.timeFrom.datetimepicker({
                locale: 'ru',
                defaultDate: from || weekAgo
            });

            this.ui.timeTo.datetimepicker({
                locale: 'ru',
                defaultDate: to || today
            });
        },

        onClickedLoadTrack: function() {
            var timeFrom = this.ui.timeFrom.data('DateTimePicker').date().unix()*1000; // utc sec
            var timeTo = this.ui.timeTo.data('DateTimePicker').date().unix()*1000; // utc sec
            
            this.onStateLoadingTrack(/*request promise*/);

            // create track
            this.model.track = new Track({
                targetId: this.model.get('id'),
                from: timeFrom,
                to: timeTo,
            });

            this.model.track.on('reset', this.onTrackReset.bind(this));
            this.model.track.startFetching(this.onFetchProgress.bind(this))
        },

        // Remove track
        onClickedUnloadTrack: function() {
            this.model.track.reset(); // Layer will be automatically removed when the track becomes empty
            this.onStateLoadTrack();
        },

        onFetchProgress: function() {
            const kbytesFetched = (this.model.track.bytesFetched / 1024).toFixed(0);
            const secondsFetched = ((this.model.track.fetchTime - this.model.track.fetchStarted) / 1000).toFixed(2);

            this.setTrackStatus(`<b>Трек скачивается</b><br>Время: ${secondsFetched} с<br>Размер: ${kbytesFetched} кб`);
        },

        onClickAddonButton: function(e) {
            if ($(e.target).hasClass('from')) {
                this.ui.timeTo.data('DateTimePicker').date(new Date());
            } else {
                this.ui.timeTo.data('DateTimePicker').date(new Date());
            }
        },

        onTrackReset: function() {
            if (this.model.track.length > 0) {

                //console.log('onTrackReset: track length is ' + this.model.track.length);

                // check if the layer is already created
                if (!this.model.trackLayer) {
                    this.model.trackLayer = new TrackLayer({
                        collection: this.model.track,
                        model: this.model
                    })
                    app.map.addLayer(this.model.trackLayer);
                }

                this.onStateLoadedTrack(this.model.track.length);

            } else {

                //console.log('onTrackReset: track is empty, removing layer');

                this.onStateTrackError(this.model.track.error);

                if (this.model.trackLayer) {
                    app.map.removeLayer(this.model.trackLayer);
                    this.model.trackLayer = null;
                }

                this.model.track.off('reset', this.onTrackReset.bind(this));
                this.model.track = null;
            }
        },

        onStateLoadTrack: function() {
            this.setTrackLoadControlsEnabled(true);
            this.setTrackManagementControlsEnabled(false);
            this.setTrackStatus('<b>Загрузите трек</b>');
        },
        
        onStateLoadingTrack: function(/*request promise*/) {
            this.setTrackLoadControlsEnabled(false);
            this.setTrackManagementControlsEnabled(false);
            this.setTrackStatus('<b>Трек подготавливается</b><br><div class="fa-2x"><i class="fa fa-spinner fa-spin"></i></div>');
        },

        onStateLoadedTrack: function(length) {
            this.setTrackLoadControlsEnabled(false);
            this.setTrackManagementControlsEnabled(true);

            // show info about loaded track
            const kbytesFetched = (this.model.track.bytesFetched / 1024).toFixed(0);
            const secondsFetched = ((this.model.track.fetchTime - this.model.track.fetchStarted) / 1000).toFixed(2);
            this.setTrackStatus(`<b>Трек загружен</b><br>Время ответа: ${secondsFetched} с<br>Размер: ${kbytesFetched} кб<br>Точек: ${length}`);

            this.highlightTrackView();
        },

        onStateTrackError: function(error) {
            this.setTrackLoadControlsEnabled(true);
            this.setTrackManagementControlsEnabled(false);
            this.setTrackStatus(error);
        },

        setTrackLoadControlsEnabled: function(val) {
            $(this.ui.timeFrom).attr('disabled', !val);
            $(this.ui.timeTo).attr('disabled', !val);
            val ? $(this.ui.load).show() : $(this.ui.load).hide();
        },

        setTrackManagementControlsEnabled: function(val) {
            val ? $(this.ui.unload).show() : $(this.ui.unload).hide();
            val ? $(this.ui.view).show() : $(this.ui.view).hide();
        },

        setTrackStatus: function(val) {
            this.ui.status.html(val);
        },

        onClickedTrackView: function(event) {

            if (this.model.trackLayer) {
                app.map.removeLayer(this.model.trackLayer);

                var view = $(event.target).attr('view') || $(event.target).parent().attr('view');
                this.model.trackLayer.view = view;

                app.map.addLayer(this.model.trackLayer);

                this.highlightTrackView();
            }
        },

        highlightTrackView() {
            this.ui.view.removeClass('btn-success').addClass('btn-default');
            $(this.el).find(`.view[view="${this.model.trackLayer.view}"]`).addClass('btn-success');
        }
    });
});
