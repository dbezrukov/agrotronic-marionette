define([
    'marionette',
    'views/map_widget',
    'views/sidebar_targets_pane',
    'collections/targets',
    'layers/targets',
    'tpl!templates/main_pane.tmpl',
    'css!stylesheets/main_pane'
    
], function(
    Marionette, 
	MapWidget, 
    TargetsPane,
    Targets,
    LayerTargets,
	template) {

	return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'main-pane',

        ui: {
        	'sidebarWidgetWrapper': '.sidebar-widget-wrapper',
        	'mapWidgetWrapper': '.map-widget-wrapper'
        },

        initialize: function() {
            var self = this;

            self.targets = new Targets;
        },

        onRender: function() {
        	var self = this;

        	self.initMap();
        	self.initSidebar();
        },

        initMap: function() {
        	var self = this;

			var layerTargets = new LayerTargets({
				collection: self.targets
			});

            var optionsOWM = { showLegend: false, opacity: 0.5, appId: 'e220da0ef978e0dea95beac16028ca30' }

            self.mapWidget = new MapWidget({
            	model: self.model,
            	userLayers: {
					'Машины': { layer: layerTargets, show: true }
				}
                //urlTiles: window.location.hostname.startsWith('gis.') ? null : 'http://' + window.location.hostname + '/maps/gmt/osm/z{z}/{y}/{x}.png'
        	});

        	self.ui.mapWidgetWrapper.append(self.mapWidget.render().el);
        },

        initSidebar: function() {
        	var self = this;

            app.map = self.mapWidget.map;

            self.sidebarWidget = new TargetsPane({
                collection: self.targets,
                mapWidget: self.mapWidget
            });

            self.ui.sidebarWidgetWrapper.append(self.sidebarWidget.render().el);
        },

        onShow: function() {
            var self = this;

            self.mapWidget.onShow();
        }
    });
});
