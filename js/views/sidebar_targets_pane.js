define([
    'underscore',
    'marionette',
    'views/targets_widget',
    'tpl!templates/sidebar_targets_pane.tmpl',
    'css!stylesheets/sidebar_targets_pane'

], function(_, Marionette, 
    TargetsWidget,
    template) {

    function multiselectData(entities) {
        var data = [];

        data.push({
            label: 'Выберите',
            value: ''
        });

        entities.forEach(function(entity) {
            data.push({
                label: entity.id,
                value: entity.id
            });
        });

        return data;
    }

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'sidebar-targets-pane',

        ui: {
            'targetsWidgetWrapper': '.targets-widget-wrapper',
            'targetFilter': '.target-filter',
            'select': '.multiselect',
        },
        
        events: {
            'keyup input.target-filter': 'targetFilterChanged',
            'click .target-clear-filter': 'targetFilterRemoved'
        },
        
        modelEvents: {
        },

        templateHelpers: {
        },

        onRender: function() {
            var self = this;
            self.targetsWidget = new TargetsWidget({
                collection: self.collection
            });

            self.ui.targetsWidgetWrapper.html(self.targetsWidget.render().el);
        },

        targetFilterChanged: function() {
            var id = this.ui.targetFilter.val();

            this.targetsWidget.filterBy(id);
        },

        targetFilterRemoved() {
            this.ui.targetFilter.val('');
            this.targetFilterChanged();
        }
    });
});
