define([
    'marionette',
    'tpl!templates/map_widget.tmpl',
    'js-cookie',
    'leaflet-groupedlayercontrol',
    'css!bower_components/bower_components/leaflet-groupedlayercontrol/dist/leaflet.groupedlayercontrol.min.css',

], function(
    Marionette, 
    template, 
    Cookies) {

    var startLevel = 12;
    var agricultureOpacity = 0.6;
    var centerLon = 37.620568;
    var centerLat = 55.754056;

    L.Map = L.Map.extend({
        openPopup: function(popup) {
            this._popup = popup;

            return this.addLayer(popup).fire('popupopen', {
                popup: this._popup
            });
        }
    });

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'widget map-widget',

        ui: {
            'map': '.map',
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;

            var boundsStr = Cookies.get('map-bounds');
            if (boundsStr) {
                var bounds = JSON.parse(boundsStr);
                var corner1 = L.latLng(bounds._northEast.lat, bounds._northEast.lng);
                var corner2 = L.latLng(bounds._southWest.lat, bounds._southWest.lng);
                self.initialBounds = L.latLngBounds(corner1, corner2);
            }
        },

        onRender: function() {

            var self = this;

            self.initMap();
        },

        initMap: function() {
            var self = this;

            var redraw = !!self.map;

            self.layerGoogle = new L.TileLayer('https://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}&s=Ga');
            self.layerOSM = new L.TileLayer(self.options.urlTiles ?
                self.options.urlTiles :
                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

            self.map = L.map(self.ui.map[0], {
                center: new L.LatLng(centerLat, centerLon),
                zoom: startLevel,
                crs: L.CRS.EPSG3857,
                layers: [self.layerGoogle],
                zoomControl: false
            });

            self.map.on('moveend', function(e) {
                var bounds = self.map.getBounds();
                Cookies.set('map-bounds', JSON.stringify(bounds), { expires: 365 });
            });
        },

        onShow: function() {
            var self = this;

            self.map.invalidateSize(false);

            // restore initial bounds
            if (self.initialBounds) {
                self.map.fitBounds(self.initialBounds);
            }

            /*
            if (self.geojsonLayer.toGeoJSON().features.length > 0) {
                self.map.fitBounds(self.geojsonLayer.getBounds());
            }
            */

            if (!self.map.searchControl) {
                addSearchControl();
            }

            if (!self.map.layersControl) {
                addLayersControl();
            }

            if (!self.map.zoomControl) {
                addZoomControl();
            }

            function addSearchControl() {
                var GoogleSearch = L.Control.extend({
                    options: {
                        position: 'topright',
                    },
                    onAdd: function() {
                        var element = document.createElement("input");
                        element.id = "searchBox";
                        element.placeholder="Поиск по карте"
                        return element;
                    }
                });

                (new GoogleSearch).addTo(self.map);

                var input = document.getElementById("searchBox");

                self.map.searchControl = new google.maps.places.SearchBox(input);

                self.map.searchControl.addListener('places_changed', function() {
                    var places = self.map.searchControl.getPlaces();

                    if (places.length == 0) {
                        return;
                    }

                    var group = L.featureGroup();

                    places.forEach(function(place) {

                        var marker = L.marker([
                            place.geometry.location.lat(),
                            place.geometry.location.lng()
                        ]);

                        group.addLayer(marker);
                    });

                    group.addTo(self.map);
                    self.map.fitBounds(group.getBounds());
                })
            }

            function addLayersControl() {
                var baseLayers = {
                    'Google': self.layerGoogle,
                    /*'OSM': self.layerOSM*/
                };

                var groupedOverlays = {};

                if (self.options.userLayers) {
                    groupedOverlays['Пользовательские слои'] = _.mapObject(self.options.userLayers, function(value, key) {
                        return value.layer;
                    })

                    // showing user layers
                    _.values(self.options.userLayers).forEach(function(userLayer) {
                        if (userLayer.show) {
                            self.map.addLayer(userLayer.layer);
                        }
                    });
                }

                self.map.layersControl = L.control.groupedLayers(baseLayers, groupedOverlays);
                self.map.layersControl.addTo(self.map);
            }

            function addZoomControl() {
                self.map.zoomControl = L.control.zoom({ position:'topright' });
                self.map.zoomControl.addTo(self.map);
            }
        },

        setLayerVisible(name, visible) {
            var self = this;

            var userLayer = self.options.userLayers[name];
            if (!userLayer) {
                console.log('layer not found: ' + name);
                return;
            }

            if (visible) {
                self.map.addLayer(userLayer.layer);
            } else {
                self.map.removeLayer(userLayer.layer);
            }
        }
    });
});