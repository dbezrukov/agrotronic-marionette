define([
    'marionette',
    'tpl!templates/target_telemetry_pane.tmpl',

], function(
    Marionette, 
    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'target-telemetry-pane no-select',

        ui: {
        },
        
        events: {
        },
        
        modelEvents: {
            'change': 'render'
        },

        templateHelpers: {
            helperLat: function() {
                return parseFloat(this.position.lat).toFixed(3);
            },
            helperLon: function() {
                return parseFloat(this.position.lon).toFixed(3);
            },
            helperUpdated: function() {
                var date = new Date(this.updated);
                var updatedLocal = date.toLocaleString();
                return updatedLocal.substr(0, updatedLocal.lastIndexOf(':'));
            },
        },

        onRender: function() {
            var self = this;
        }
    });
});
