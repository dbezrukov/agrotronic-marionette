define([
    'marionette',
    'tpl!templates/navbar_widget.tmpl',
    'bootstrap'

], function(
    Marionette, 
    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'nav',
        className: 'navbar navbar-default navbar-static-top',

        ui: {
        	navbarModes: '#navbarModes',
        	navbarWidgets: '#navbarWidgets'
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;

            app.vent.on('page:change', self.onPageChanged, self);
        },

        onRender: function() {
        	var self = this;

			self.initModes();
			self.initWidgets();
			self.initColor();

            if ($(self.el).localize) {
                $(self.el).localize();
            }

			app.vent.on('page:change', self.onPageChanged, self);
        },

        initModes: function() {
        	var self = this;

        	if (self.options.modes) {
				self.options.modes.forEach(function(li) {

					$(self.ui.navbarModes).append(_.template(li)(self.model.toJSON()));
				})
			}
        },

        initWidgets: function() {
        	var self = this;

        	if (self.options.widgets) {
				self.options.widgets.forEach(function(li) {
					
					$(self.ui.navbarWidgets).append(li);
				})
			}
        },

        initColor: function() {
        	var self = this;
        },

        onPageChanged: function(page) {
        	var self = this;

			$(self.ui.navbarModes).find('.active').removeClass("active");

        	var navItem = $(self.ui.navbarModes).find("a[href='/" + page.split('/')[1] + "']");
        	if (navItem.length > 0) {
        		navItem.parent().addClass("active");
        	} else {
        		$(self.ui.navbarModes).children().removeClass("active");
        	}
        }
    });
});